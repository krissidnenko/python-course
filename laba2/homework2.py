print("Функція номер 2")
def miles_to_feet(x):
    print("Miles -", x, ", feets: =", x*5280)
miles_to_feet(2)
miles_to_feet(3)
miles_to_feet(4)
miles_to_feet(5)
x=9
miles_to_feet((lambda x: a*5280)(a))

numbers = lambda a,b: a%b
print("Цел. остаток от деления",numbers(26,7))

print("-------------------------")

print("Позиційні аргументи")
def cars(sportcar,crossover,electric):
    print("Sportcar:", sportcar, ", Crossover:", crossover, ", Electric car:", electric)
cars("Porsche 911","Honda Pilot", "Tesla")

print("-------------------------")

print("Передача аргументів іменем")
def cars(sportcar,crossover,electric):
    print("Sportcar:", sportcar, ", Crossover:", crossover, ", Electric car:", electric)
cars(crossover="Honds Pilot", electric="Tesla", sportcar="Porsche 911")

print("-------------------------")

print("Параметри за замовчуванням")
def example(x, y=6, z=2):
    return x+y-z
print(example(4,5,1))
print(example(1,7))
print(example(7))
print(example)